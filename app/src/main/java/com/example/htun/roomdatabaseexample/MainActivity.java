package com.example.htun.roomdatabaseexample;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<BookEntity> bookList;
    private EditText id,auth,title;
    private Button button;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id=findViewById(R.id.idEd);
        title=findViewById(R.id.titleEd);
        auth=findViewById(R.id.authEd);
        button=findViewById(R.id.button);
        textView=findViewById(R.id.textView);

        final AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"production")
                .build();

        // BookDao ကနေ getAllBooks method ကို ခေါ်လိုက်ရင် book list ပါဝင်တဲ့ LiveData ရပါတယ်။
        LiveData<List<BookEntity>> liveBookList = AppDatabase.getInstance(this).bookDao().getAllBooks();

        // အဲ့ဒီ LiveData ကို observe လုပ်ရပါတယ်။
        liveBookList.observe(MainActivity.this, new Observer<List<BookEntity>>() {
            @Override
            public void onChanged(@Nullable List<BookEntity> bookEntities) {
                bookList = bookEntities;
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final BookEntity book=new BookEntity(1,title.getText().toString(),auth.getText().toString());
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        db.bookDao().addBook(book);
                    bookList.add(book);}
                });
                Toast.makeText(MainActivity.this,bookList.size(),Toast.LENGTH_SHORT).show();

//                textView.setText(db.bookDao().getAllBooks().getValue().toString());

                for (int i=0;i<bookList.size();i++){
                    textView.setText(String.valueOf(bookList.get(i).getId()).toString()+"  "+bookList.get(i).getTitle().toString()+"  "+bookList.get(i).getAuthorName().toString());
                }
            }
        });

    }


}
