package com.example.htun.roomdatabaseexample;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {BookEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;

    // Database name
    public static final String DATABASE_NAME = "books-example-db";

    // All DAOs of the tables must be added here.
    public abstract BookDao bookDao();

    // Checks if an instance already exists, and if not, creates a new instance and returns it.
    public static AppDatabase getInstance(final Context context) {
        if(instance == null){
            instance = buildDatabase(context.getApplicationContext());
        }
        return instance;
    }

    private static AppDatabase buildDatabase(final Context appContext) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME).build();
    }
}
