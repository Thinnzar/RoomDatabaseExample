package com.example.htun.roomdatabaseexample;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
@Entity(tableName = "books")
public class BookEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String authorName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public BookEntity(int id, String title, String authorName) {

        this.id = id;
        this.title = title;
        this.authorName = authorName;
    }
}
