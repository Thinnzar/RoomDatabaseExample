package com.example.htun.roomdatabaseexample;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BookDao {
    // returns book list as LiveData
    @Query("SELECT * from books")
    LiveData<List<BookEntity>> getAllBooks();

    // returns book as LiveData
    @Query("SELECT * from books where id = :bookId")
    LiveData<BookEntity> getBook(int bookId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addBook(BookEntity bookEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addBooks(List<BookEntity> bookEntities);

    @Query("DELETE from books")
    void deleteAllBooks();
}
